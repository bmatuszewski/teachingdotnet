﻿using System;
using System.Linq;
using System.Reflection;

namespace Teaching.Core
{
    internal static class ReflectionHelper
    {
        public static T[] GetAllImplementationsOf<T>()
        {
            var allTypes = Assembly.GetAssembly(typeof(Program)).GetTypes();
            var demoImplementataions = allTypes.Where(typeof(T).IsAssignableFrom).Where(t => t.IsClass);
            var demos = demoImplementataions.Select(t => (T)Activator.CreateInstance(t)).OrderBy(d => d.GetType().Name);

            return demos.ToArray();
        }
    }
}
