﻿using System;
using System.Diagnostics;
using System.Linq;

namespace Teaching.Core
{
    static class Program
    {
        static void Main()
        {
            ConsoleHelper.SetDefaultColor();
            ShowMenu(ReflectionHelper.GetAllImplementationsOf<IDemo>());
            ConsoleHelper.SetDefaultColor();
        }

        private static void ShowMenu(IDemo[] demos)
        {
            string[] exitCommands = { "exit", "quit", "bye", "0", "q", "x" };

            do
            {
                PrintDemoOptions(demos);
                var command = ConsoleHelper.HandleUserInput();

                if (exitCommands.Contains(command))
                {
                    return;
                }

                DispatchCommand(demos, command);
            } while (true);
        }

        private static void DispatchCommand(IDemo[] demos, string command)
        {
            int chosenIndex;
            var handled = false;

            if (int.TryParse(command, out chosenIndex))
            {
                if (chosenIndex > 0 && chosenIndex <= demos.Length)
                {
                    var watch = Stopwatch.StartNew();
                    ShowDemoHeader(demos, chosenIndex);

                    try
                    {
                        demos[chosenIndex - 1].Go();
                    }
                    catch (Exception ex)
                    {
                        SummarizeError(ex);
                    }

                    ShowDemoFooter(watch);
                    watch.Stop();
                    handled = true;
                }
            }

            if (!handled)
            {
                Console.WriteLine("Unknown command.");
                Console.WriteLine();
            }
        }

        private static void SummarizeError(Exception ex)
        {
            ConsoleHelper.SetErrorColor();
            Console.WriteLine("There was an error in your code:");
            Console.WriteLine(ex);
        }

        private static void ShowDemoFooter(Stopwatch watch)
        {
            ConsoleHelper.SetDefaultColor();
            Console.WriteLine();
            Console.WriteLine($"Ended in {watch.ElapsedMilliseconds}ms (Ticks:{watch.ElapsedTicks})");
            Console.WriteLine();
        }

        private static void ShowDemoHeader(IDemo[] demos, int chosenIndex)
        {
            Console.WriteLine($"Running {demos[chosenIndex - 1].GetType().Name}...");
            Console.WriteLine();
            ConsoleHelper.SetActionColor();
        }

        private static void PrintDemoOptions(IDemo[] demos)
        {
            Console.WriteLine("== MENU =======================");

            for (int i = 1; i <= demos.Length; ++i)
            {
                Console.WriteLine($"{i,-3}: {demos[i - 1].GetType().Name}");
            }
        }
    }
}
