﻿using System;

namespace Teaching.Core
{
    internal static class ConsoleHelper
    {
        public static void SetDefaultColor()
        {
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        public static void SetActionColor()
        {
            Console.ForegroundColor = ConsoleColor.Green;
        }

        public static void SetErrorColor()
        {
            Console.ForegroundColor = ConsoleColor.Red;
        }

        public static string HandleUserInput()
        {
            Console.Write("> ");
            var command = Console.ReadLine();
            return command;
        }
    }
}
