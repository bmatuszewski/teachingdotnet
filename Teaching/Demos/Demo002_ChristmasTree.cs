﻿using System;
using Teaching.Core;

namespace Teaching.Demos
{
    class Demo002_ChristmasTree : IDemo
    {
        public void Go()
        {
            // NEW THINGS: variable, for
            Console.WriteLine("Basics - chirtmas tree");

            int allLines = 10;

            for (int lineNumber = 0; lineNumber < allLines; lineNumber++)
            {
                Console.WriteLine("*");
            }
        }
    }
}
