﻿using System;
using Teaching.Core;

namespace Teaching.Demos
{
    class Demo001_HelloWorld : IDemo
    {
        public void Go()
        {
            // NEW THINGS: file structure, writeline
            Console.WriteLine("Hello, world!");
        }
    }
}
