﻿using System;
using Teaching.Core;

namespace Teaching.Demos
{
    class Demo006_IsPrimeNumber2 : IDemo
    {
        public void Go()
        {
            // NEW THINGS: tryparse, out, return, sqrt, cast double <-> int implicit+ explicit
            Console.WriteLine("Basics - Is prime number 2");

            Console.Write("Enter number: ");
            var text = Console.ReadLine();

            int number;
            if (!int.TryParse(text, out number))
            {
                Console.WriteLine("This is not a number");
                return;
            }

            int sqrtOfNumber = (int)Math.Sqrt(number);

            for (int i = 2; i <= sqrtOfNumber; i++)
            {
                if (number % i == 0)
                {
                    Console.WriteLine(number + " is NOT a prime");
                    return;
                }
            }

            Console.WriteLine(number + " is a prime");
        }
    }
}
