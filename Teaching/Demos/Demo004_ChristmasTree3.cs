﻿using System;
using Teaching.Core;

namespace Teaching.Demos
{
    class Demo004_ChristmasTree3 : IDemo
    {
        public void Go()
        {
            // NEW THINGS: division, little tricks
            Console.WriteLine("Basics - chirtmas tree 3");

            int allLines = 10;

            for (int lineNumber = 0; lineNumber < allLines; lineNumber+=2)
            {
                int spaces = 10 - lineNumber / 2;

                for (int space = 0; space < spaces; space++)
                {
                    Console.Write(" ");
                }

                for (int star = 1; star <= lineNumber + 1; star++)
                {
                    Console.Write("*");
                }

                Console.WriteLine();
            }
        }
    }
}
