﻿using System;
using Teaching.Core;

namespace Teaching.Demos
{
    class Demo003_ChristmasTree2 : IDemo
    {
        public void Go()
        {
            // NEW THINGS: nested for
            Console.WriteLine("Basics - chirtmas tree 2");

            int allLines = 10;

            for (int lineNumber = 0; lineNumber < allLines; lineNumber++)
            {
                for (int star = 1; star <= lineNumber+1; star++)
                {
                    Console.Write("*");
                }

                Console.WriteLine();
            }
        }
    }
}
