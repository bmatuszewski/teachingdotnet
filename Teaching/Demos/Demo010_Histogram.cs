﻿using System;
using Teaching.Core;

namespace Teaching.Demos
{
    class Demo010_Histogram : IDemo
    {
        public void Go()
        {
            // NEW THINGS: arrays
            Console.WriteLine("Arrays - Drawing histogram 2,4,6,10,1");

            int[] values = { 2, 4, 6, 10, 1 };

            for (int lineNumber = 0; lineNumber < values.Length; lineNumber++)
            {
                for (int star = 0; star < values[lineNumber]; star++)
                {
                    Console.Write("*");
                }

                Console.WriteLine();
            }
        }
    }
}
