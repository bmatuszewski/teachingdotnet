﻿using System;
using Teaching.Core;

namespace Teaching.Demos
{
    class Demo005_IsPrimeNumber : IDemo
    {
        public void Go()
        {
            // NEW THINGS: if, modulo, parse, readline, text concatenation, else
            Console.WriteLine("Basics - Is prime number - brutal method");

            Console.Write("Enter number: ");
            var text = Console.ReadLine();
            int number = int.Parse(text);
            bool isPrime = true;

            for (int i = number - 1; i >= 2; i--)
            {
                if (number % i == 0)
                {
                    isPrime = false;
                }
            }

            if (isPrime)
            {
                Console.WriteLine(number + " is a prime");
            }
            else
            {
                Console.WriteLine(number + " is NOT a prime");
            }
        }
    }
}
